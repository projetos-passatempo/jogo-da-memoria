const cardCountryData = 
[
  {
    "id" : 1,
    "name" : "Alemanha",
    "flag" : "alemanha.png"
  },
  {
    "id" : 2,
    "name" : "Argentina",
    "flag" : "argentina.png"
  },
  {
    "id" : 3,
    "name" : "Austrália",
    "flag" : "australia.png"
  },
  {
    "id" : 4,
    "name" : "Brasil",
    "flag" : "brasil.png"
  },
  {
    "id" : 5,
    "name" : "China",
    "flag" : "china.png"
  },
  {
    "id" : 6,
    "name" : "Coreia do Sul",
    "flag" : "coreia-do-sul.png"
  },
  {
    "id" : 7,
    "name" : "Espanha",
    "flag" : "espanha.png"
  },
  {
    "id" : 8,
    "name" : "Itália",
    "flag" : "italia.png"
  },
  {
    "id" : 9,
    "name" : "Japão",
    "flag" : "japao.png"
  },
  {
    "id" : 10,
    "name" : "Paraguai",
    "flag" : "paraguai.png"
  },
  {
    "id" : 11,
    "name" : "Portugal",
    "flag" : "portugal.png"
  },
  {
    "id" : 12,
    "name" : "Uruguai",
    "flag" : "uruguai.png"
  }
]

const cardsState = []