/*
  Resultado esperado.

  <div class="row">
    <div class="col">
      <div class="card card-game">
        <div class="overlay">
          <span>1</span>
        </div>
        <img class="card-img-top" src="img/brasil.png" alt="Card image cap">
        <div class="card-body">
          <p class="card-text"><strong>BRASIL</strong></p>
        </div>
      </div>
    </div>
  </div>
*/

var rows = 4, cols = 6
var gameBoard = document.getElementById("game")

function createRow(slicedCountryDataCols) {
  let rowCreated = document.createElement("div")
  rowCreated.className = "row"
  gameBoard.appendChild(rowCreated)
  slicedCountryDataCols.forEach(col => rowCreated.appendChild(col))
}

function createCardOverlay(cardCreated, index) {
  let overlay = document.createElement("div")
  overlay.className = "overlay justify-content-center align-items-center"
  overlay.id = index

  cardCreated.addEventListener("click", () => {
    overlay.style = "display: block;"
    registerClick(cardCreated, overlay)
  })
  overlay.addEventListener("click", evt => {
    evt.stopPropagation()
    overlay.style = "display: none;"
    registerClick(cardCreated, overlay)
  })

  let span = document.createElement("span")
  span.textContent = index
  overlay.appendChild(span)
  cardCreated.appendChild(overlay)
}

function createCard(colCreated, country, cardId) {
  if(_isCol(colCreated)) {
    
    let cardCreated = document.createElement("div")
    cardCreated.className = "card card-game"
    cardCreated.id = cardId

    _createCardImg(cardCreated, country)
    _createCardBody(cardCreated, country)

    colCreated.appendChild(cardCreated)
  } else {
    alert("Erro ao adicionar card: elemento pai não é válido.")
  }
}

function generateStandardPairOfCards(country) {
  let cardPair = []
  for(let i = 0; i < 2; i++) {
    const identifier = ['a','b']
    let columnCreated = document.createElement("div")
    columnCreated.className = "col"
    let cardId = `${country.id}${identifier[i]}`
    createCard(columnCreated, country, cardId)
    cardPair.push(columnCreated)
  }
  return cardPair
}

function _createCardImg(card, country) {
  if(__isCard(card)) {
    let cardImg = document.createElement("img")
    cardImg.alt = "Card Jogo"
    cardImg.src = "img/" + country.flag
    cardImg.className = "card-img-top"
    card.appendChild(cardImg)
  }
}

function _createCardBody(card, country) {
  if(__isCard(card)) {
    let cardBody = document.createElement("div")
    cardBody.className = "card-body"

    let cardBodyText = document.createElement("p")
    cardBodyText.textContent = country.name

    cardBody.appendChild(cardBodyText)
    card.appendChild(cardBody)
  }
}

function _isRow(elementToBeChecked) {
  return __isHtmlDivElement(elementToBeChecked) && 
  elementToBeChecked.className == "row"
}

function _isCol(elementToBeChecked) {
  return __isHtmlDivElement(elementToBeChecked) && 
  elementToBeChecked.className == "col"
}

function __isCard(elementToBeChecked) {
  return __isHtmlDivElement(elementToBeChecked) && 
  elementToBeChecked.className == "card card-game"
}

function __isHtmlDivElement(elementToBeChecked) {
  return typeof elementToBeChecked == 'object' && 
  elementToBeChecked instanceof HTMLDivElement
}

function createGameboardItems() {
  
  let listOfCards = []
  cardCountryData.forEach(country => listOfCards.push(generateStandardPairOfCards(country)))

  let shuffledCards = scrambleCards(listOfCards)

  shuffledCards.forEach((card, index) => createCardOverlay(card, (index + 1)))

  let gameBoardItems = []
  console.log(`Gerando um jogo com ${rows * cols} cards (${(rows * cols) / 2} pares)`)
  let numberOfShuffledCards = shuffledCards.length

  for(let i = 0; i < rows; i++) {
    let slicedCountryDataCols = shuffledCards.slice(((numberOfShuffledCards / rows) * i), (numberOfShuffledCards / rows) * (i + 1))
    gameBoardItems.push(createRow(slicedCountryDataCols))
  }
  
  return gameBoardItems
}