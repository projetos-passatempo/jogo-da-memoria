var firstClicked = [], secondClicked = []
var points = 0, misses = 0
var multiPoints = 1, multiMisses = 1

function registerClick(card, overlay) {

  if(firstClicked.length === 0) {
    firstClicked = [card, overlay]
    return
  }

  if(firstClicked.length !== 0 && secondClicked.length === 0) {
    if(firstClicked[0].id !== card.id) {
      secondClicked = [card, overlay]
      if(_isMatch(firstClicked[0].id, secondClicked[0].id)) {
        points = points + 10 * multiPoints
        multiPoints = multiPoints + 1
        multiMisses = 1
        document.getElementById("points").textContent = points
        keepCardsOpen(firstClicked, secondClicked)
      } else {
        misses = misses + 1
        points = points === 0 ? 0 : points - 1 * multiMisses
        multiMisses = multiMisses + 1
        multiPoints = 1
        document.getElementById("misses").textContent = misses
        document.getElementById("points").textContent = points
        closeCards(firstClicked, secondClicked)
      }
      firstClicked = []
      secondClicked = []
    } else {
      keepOpen(firstClicked)
    }
    return
  }
}

function _isMatch(firstClickedCardId, secondClickedCardId) {
  let comparatorOne = firstClickedCardId.substring(0, firstClickedCardId.length - 1)
  let comparatorTwo = secondClickedCardId.substring(0, secondClickedCardId.length - 1)
  return comparatorOne === comparatorTwo
}

function isOpen(overlay) {
  return overlay.style.display === 'none'
}

function closeCards(firstClicked, secondClicked) {
  document.body.style = "pointer-events: none;"
  setTimeout(() => {
    firstClicked[1].style = "display: block;"
    secondClicked[1].style = "display: block;"
    document.body.style = "pointer-events: initial;"
  }, 1000);
}

function keepOpen(firstClicked) {
  firstClicked[1].style = "display: none;"
}

function keepCardsOpen(firstClicked, secondClicked) {
  firstClicked[1].style = "display: block; background-color: rgba(0,0,0,0.5); pointer-events: none;"
  firstClicked[0].style = "pointer-events: none;"
  secondClicked[1].style = "display: block; background-color: rgba(0,0,0,0.5); pointer-events: none;"
  secondClicked[0].style = "pointer-events: none;"
}

function updateScore() {
  document.getElementById("misses")
}