function scrambleCards(gameBoardItems) {

  let columns = gameBoardItems.flat()
  let columnsToShuffle = []

  columns.forEach(row => {
    const rowItems = [...row.children]
    columnsToShuffle.push(rowItems)
  })
  
  return _shuffle(columnsToShuffle.flat())
}

function _shuffle(items) {
  if(Array.isArray(items)) {
    let currentIndex = items.length,  randomIndex
    while (currentIndex != 0) {
      randomIndex = Math.floor(Math.random() * currentIndex)
      currentIndex--
      [items[currentIndex], items[randomIndex]] = [items[randomIndex], items[currentIndex]]
    }
    return items
  } else {
    alert("Não foi possível embaralhar. Argumento não corresponde a um array.")
  }
}