### Jogo da Memória

## Sobre o jogo

O jogo da memória consiste em descobrir todos os pares de cartões virados com a menor quantidade de movimentos possível.

A partida acaba quando o jogador descobre todos os pares.

### Cálculo da pontuação

Este projeto calcula a pontuação da seguinte forma:

   - Acerto: acrescenta-se 10 pontos, sendo este valor multiplicado pelo número de acertos consecutivos conforme estes ocorrerem.
   - Erro: subtrai-se 1 ponto da pontuação geral se esta for maior que 0. O valor do desconto é multiplicado pelo número de erros consecutivos conforme estes ocorrerem. 

## Sobre o projeto

O projeto foi desenvolvido sob o princípio da simplicidade, de forma a não utilizar nenhum recurso server-side.

Basta abrir o arquivo `index.html` e começar a diversão.

Eu sei que é possível inspecionar a página e descobrir de antemão quais são os pares, mas, não se esqueça de que este
projeto é meramente um passatempo, um exercício de lógica e (por que não?) um meio para se divertir sozinho ou com amigos.